# Build
```./gradlew build test```

# Run
```./gradlew bootRun```

# Use
```POST localhost:8080/api/v1/sea-cleaning/new```
```
{
  "areaSize" : [7, 5],
  "startingPosition" : [1, 2],
  "oilPatches" : [
    [1, 0],
    [2, 2],
    [2, 3]
  ],
  "navigationInstructions" : "NNESEESWNWW"
}
```
