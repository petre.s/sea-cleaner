package com.petrestanaringa.seacleaner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeaCleanerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeaCleanerApplication.class, args);
	}

}
