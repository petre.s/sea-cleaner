package com.petrestanaringa.seacleaner.services;

import com.petrestanaringa.seacleaner.seacleaning.CleanerResult;
import com.petrestanaringa.seacleaner.seacleaning.Coordinates;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface CleaningService {

    CleanerResult applyCleaning(Coordinates area, Coordinates startingPosition,
            List<Coordinates> oilPatches, String navigationInstructions);
}
