package com.petrestanaringa.seacleaner.services.impl;

import com.petrestanaringa.seacleaner.seacleaning.CleanerResult;
import com.petrestanaringa.seacleaner.seacleaning.Coordinates;
import com.petrestanaringa.seacleaner.seacleaning.SeaCleaner;
import com.petrestanaringa.seacleaner.services.CleaningService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class CleaningServiceImpl implements CleaningService {

    @Override
    public CleanerResult applyCleaning(Coordinates area, Coordinates startingPosition,
            List<Coordinates> oilPatches, String navigationInstructions) {

        SeaCleaner cleaner = new SeaCleaner(area, startingPosition, oilPatches, navigationInstructions);
        return cleaner.getResult();
    }
}
