package com.petrestanaringa.seacleaner.api;

import com.petrestanaringa.seacleaner.seacleaning.CleanerResult;
import com.petrestanaringa.seacleaner.seacleaning.Coordinates;
import com.petrestanaringa.seacleaner.dto.CleanerRequestDto;
import com.petrestanaringa.seacleaner.dto.CleanerResultDto;
import com.petrestanaringa.seacleaner.exceptions.OilCleaningException;
import com.petrestanaringa.seacleaner.services.CleaningService;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/sea-cleaning")
public class SeaCleanerController {

    private CleaningService cleaningService;

    @Autowired
    public SeaCleanerController(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    @ExceptionHandler(OilCleaningException.class)
    public ResponseEntity badRequestExceptionMapping(OilCleaningException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @PostMapping("/new")
    public CleanerResultDto applyCleaner(@RequestBody CleanerRequestDto cleanerRequestDto) {
        validateRequest(cleanerRequestDto);

        Coordinates startingPosition = new Coordinates(
                cleanerRequestDto.getStartingPosition().get(0),
                cleanerRequestDto.getStartingPosition().get(1));

        List<Coordinates> oilPatches = cleanerRequestDto.getOilPatches().stream()
                .map(p -> new Coordinates(p.get(0), p.get(1))).collect(
                        Collectors.toList());

        Coordinates area = new Coordinates(cleanerRequestDto.getAreaSize().get(0),
                cleanerRequestDto.getAreaSize().get(1));

        CleanerResult cleanerResult = cleaningService.applyCleaning(area, startingPosition, oilPatches,
                cleanerRequestDto.getNavigationInstructions());

        return getCleanerResultDto(cleanerResult);

    }

    private CleanerResultDto getCleanerResultDto(CleanerResult cleanerResult) {
        return new CleanerResultDto(
                Arrays.asList(cleanerResult.getFinalPosition().getX(), cleanerResult.getFinalPosition().getY()),
                cleanerResult.getOilPatchesCleaned());
    }

    private void validateRequest(CleanerRequestDto cleanerRequestDto) {
        if (cleanerRequestDto.getAreaSize() == null ||
                cleanerRequestDto.getAreaSize().size() != 2 ||
                cleanerRequestDto.getNavigationInstructions() == null ||
                cleanerRequestDto.getOilPatches() == null ||
                cleanerRequestDto.getOilPatches().stream().anyMatch(p -> p.size() != 2) ||
                cleanerRequestDto.getStartingPosition() == null) {
            throw new OilCleaningException("Invalid request");
        }
    }
}
