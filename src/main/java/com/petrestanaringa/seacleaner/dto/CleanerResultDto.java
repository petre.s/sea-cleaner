package com.petrestanaringa.seacleaner.dto;

import java.util.List;

public class CleanerResultDto {
    private List<Integer> finalPosition;
    private int oilPatchesCleaned;

    public CleanerResultDto(List<Integer> finalPosition, int oilPatchesCleaned) {
        this.finalPosition = finalPosition;
        this.oilPatchesCleaned = oilPatchesCleaned;
    }

    public List<Integer> getFinalPosition() {
        return finalPosition;
    }

    public int getOilPatchesCleaned() {
        return oilPatchesCleaned;
    }
}
