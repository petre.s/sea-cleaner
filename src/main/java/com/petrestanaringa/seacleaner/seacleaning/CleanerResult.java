package com.petrestanaringa.seacleaner.seacleaning;

public class CleanerResult {
    private Coordinates finalPosition;
    private int oilPatchesCleaned;

    public CleanerResult(Coordinates finalPosition, int oilPatchesCleaned) {
        this.finalPosition = finalPosition;
        this.oilPatchesCleaned = oilPatchesCleaned;
    }

    public Coordinates getFinalPosition() {
        return finalPosition;
    }

    public int getOilPatchesCleaned() {
        return oilPatchesCleaned;
    }
}
