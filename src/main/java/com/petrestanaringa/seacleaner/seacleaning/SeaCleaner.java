package com.petrestanaringa.seacleaner.seacleaning;

import com.petrestanaringa.seacleaner.exceptions.OilCleaningException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class SeaCleaner {

    private int areaSizeX;
    private int areaSizeY;
    private Coordinates startingPosition;
    private Set<Coordinates> oilPatches;
    private String navigationInstructions;
    private CleanerResult result;
    private static final HashMap<Character, Coordinates> NAVIGATION_INCREMENT;

    static {
        NAVIGATION_INCREMENT = new HashMap<>();
        NAVIGATION_INCREMENT.put('N', new Coordinates(0, 1));
        NAVIGATION_INCREMENT.put('S', new Coordinates(0, -1));
        NAVIGATION_INCREMENT.put('E', new Coordinates(1, 0));
        NAVIGATION_INCREMENT.put('W', new Coordinates(-1, 0));
    }

    public SeaCleaner(Coordinates area, Coordinates startingPosition, List<Coordinates> oilPatches,
            String navigationInstructions) {
        Objects.requireNonNull(area);
        Objects.requireNonNull(oilPatches);

        this.areaSizeX = area.getX();
        this.areaSizeY = area.getY();

        if (areaSizeX <= 0 || areaSizeY <= 0){
            throw new OilCleaningException("Area size must be greater than 0");
        }

        this.navigationInstructions = navigationInstructions.toUpperCase();

        for (Coordinates oilPatch : oilPatches) {
            if (!isInBoundaries(oilPatch.getX(), oilPatch.getY())) {
                throw new OilCleaningException("Oil patch not in boundaries: " + oilPatch);
            }
        }

        if (!isInBoundaries(startingPosition.getX(), startingPosition.getY())) {
            throw new OilCleaningException("Initial position is out of boundaries");
        }

        this.startingPosition = startingPosition;
        this.oilPatches = new HashSet<>(oilPatches);

        compute();
    }

    private void compute() {
        int x = startingPosition.getX();
        int y = startingPosition.getY();

        int cleanedPatched = 0;
        for (int i = 0; i < navigationInstructions.length(); i++) {
            char instruction = navigationInstructions.charAt(i);
            Coordinates increment = NAVIGATION_INCREMENT.get(instruction);

            if (null == increment) {
                throw new OilCleaningException(getUnknownNavigationMessage(instruction));
            }

            x += increment.getX();
            y += increment.getY();

            if (!isInBoundaries(x, y)) {
                throw new OilCleaningException(getOutOfBoundariesMessage(i));
            }

            boolean existed = oilPatches.remove(new Coordinates(x, y));
            if (existed) {
                cleanedPatched++;
            }
        }
        result = new CleanerResult(new Coordinates(x, y), cleanedPatched);
    }

    private String getUnknownNavigationMessage(char instruction) {
        return "Unknown navigation instruction: " + instruction;
    }

    private String getOutOfBoundariesMessage(int i) {
        return "Out of boundaries after executing " + navigationInstructions.substring(0, i + 1);
    }

    private boolean isInBoundaries(int x, int y) {
        return x >= 0 && y >= 0 && x < areaSizeX && y < areaSizeY;
    }

    public CleanerResult getResult() {
        return result;
    }
}
