package com.petrestanaringa.seacleaner.exceptions;

public class OilCleaningException extends RuntimeException {

    public OilCleaningException(String message) {
        super(message);
    }
}
