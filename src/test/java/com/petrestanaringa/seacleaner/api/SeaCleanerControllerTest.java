package com.petrestanaringa.seacleaner.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.petrestanaringa.seacleaner.seacleaning.CleanerResult;
import com.petrestanaringa.seacleaner.seacleaning.Coordinates;
import com.petrestanaringa.seacleaner.dto.CleanerRequestDto;
import com.petrestanaringa.seacleaner.dto.CleanerResultDto;
import com.petrestanaringa.seacleaner.exceptions.OilCleaningException;
import com.petrestanaringa.seacleaner.services.CleaningService;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SeaCleanerControllerTest {

    @Mock
    CleaningService cleaningService;

    CleanerResult cleanerResult = new CleanerResult(new Coordinates(3, 4), 4);

    @Before
    public void arrange() {
        when(cleaningService.applyCleaning(any(), any(), any(), any())).thenReturn(cleanerResult);
    }

    @Test
    public void applyCleanerTestSuccesCase() {
        // Arrange
        SeaCleanerController seaCleanerController = new SeaCleanerController(cleaningService);
        CleanerRequestDto cleanerRequestDto = new CleanerRequestDto(Arrays.asList(3, 3), Arrays.asList(0, 0),
                Collections.emptyList(), "NS");

        // Act
        CleanerResultDto cleanerResultDto = seaCleanerController.applyCleaner(cleanerRequestDto);

        // Assert
        Assert.assertEquals(4, cleanerResultDto.getOilPatchesCleaned());
        Assert.assertEquals(2, cleanerResultDto.getFinalPosition().size());
        Assert.assertEquals(3, (int) cleanerResultDto.getFinalPosition().get(0));
        Assert.assertEquals(4, (int) cleanerResultDto.getFinalPosition().get(1));
    }

    @Test(expected = OilCleaningException.class)
    public void applyCleanerTestBadRequestMissingArea() {
        // Arrange
        SeaCleanerController seaCleanerController = new SeaCleanerController(cleaningService);
        CleanerRequestDto cleanerRequestDto = new CleanerRequestDto(null, Arrays.asList(0, 0),
                Collections.emptyList(), "NS");

        // Act
        CleanerResultDto cleanerResultDto = seaCleanerController.applyCleaner(cleanerRequestDto);
    }

    @Test(expected = OilCleaningException.class)
    public void applyCleanerTestBadRequestMissingStartPosition() {
        // Arrange
        SeaCleanerController seaCleanerController = new SeaCleanerController(cleaningService);
        CleanerRequestDto cleanerRequestDto = new CleanerRequestDto(Arrays.asList(3, 3), null,
                Collections.emptyList(), "NS");

        // Act
        CleanerResultDto cleanerResultDto = seaCleanerController.applyCleaner(cleanerRequestDto);
    }

    @Test(expected = OilCleaningException.class)
    public void applyCleanerTestBadRequestMissingAllFields() {
        // Arrange
        SeaCleanerController seaCleanerController = new SeaCleanerController(cleaningService);
        CleanerRequestDto cleanerRequestDto = new CleanerRequestDto(null, null,
                null, null);

        // Act
        CleanerResultDto cleanerResultDto = seaCleanerController.applyCleaner(cleanerRequestDto);
    }

}
