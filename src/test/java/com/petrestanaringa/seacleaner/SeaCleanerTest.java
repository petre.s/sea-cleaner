package com.petrestanaringa.seacleaner;

import com.petrestanaringa.seacleaner.exceptions.OilCleaningException;
import com.petrestanaringa.seacleaner.seacleaning.CleanerResult;
import com.petrestanaringa.seacleaner.seacleaning.Coordinates;
import com.petrestanaringa.seacleaner.seacleaning.SeaCleaner;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class SeaCleanerTest {

    @Test
    public void cleanerNormalCase() {
        // Arrange
        Coordinates startingPosition = new Coordinates(0, 0);
        List<Coordinates> oilPatchesList = Arrays
                .asList(new Coordinates(1, 1), new Coordinates(2, 1), new Coordinates(4, 1));

        SeaCleaner seaCleaner = new SeaCleaner(new Coordinates(8, 2), startingPosition, oilPatchesList, "NEE");

        // Act
        CleanerResult result = seaCleaner.getResult();

        // Assert

        Assert.assertEquals(2, result.getOilPatchesCleaned());
        Assert.assertEquals(2, result.getFinalPosition().getX());
        Assert.assertEquals(1, result.getFinalPosition().getY());
    }

    @Test
    public void cleanerEmptyNavigation() {
        // Arrange
        Coordinates startingPosition = new Coordinates(0, 0);
        List<Coordinates> oilPatchesList = Collections.singletonList(new Coordinates(1, 1));

        SeaCleaner seaCleaner = new SeaCleaner(new Coordinates(8, 2), startingPosition, oilPatchesList, "");

        // Act
        CleanerResult result = seaCleaner.getResult();

        // Assert

        Assert.assertEquals(0, result.getOilPatchesCleaned());
        Assert.assertEquals(0, result.getFinalPosition().getX());
        Assert.assertEquals(0, result.getFinalPosition().getY());
    }

    @Test(expected = OilCleaningException.class)
    public void cleanerNavigateOut() {
        // Arrange
        Coordinates startingPosition = new Coordinates(0, 0);
        List<Coordinates> oilPatchesList = Collections.singletonList(new Coordinates(1, 1));

        // Act
        SeaCleaner seaCleaner = new SeaCleaner(new Coordinates(2, 2), startingPosition, oilPatchesList, "NNNN");
    }

    @Test(expected = OilCleaningException.class)
    public void cleanerStartPositionOut() {
        // Arrange
        Coordinates startingPosition = new Coordinates(10, 0);
        List<Coordinates> oilPatchesList = Collections.singletonList(new Coordinates(1, 1));

        // Act
        SeaCleaner seaCleaner = new SeaCleaner(new Coordinates(2, 2), startingPosition, oilPatchesList, "N");
    }

    @Test(expected = OilCleaningException.class)
    public void cleanerUnknownNavigation() {
        // Arrange
        Coordinates startingPosition = new Coordinates(0, 0);
        List<Coordinates> oilPatchesList = Collections.singletonList(new Coordinates(1, 1));

        // Act
        SeaCleaner seaCleaner = new SeaCleaner(new Coordinates(2, 2), startingPosition, oilPatchesList, "Hello");
    }
}
